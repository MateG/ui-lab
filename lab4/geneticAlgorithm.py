import numpy as np 

class GeneticAlgorithm(object): 
	"""
		Implement a simple generationl genetic algorithm as described in the instructions
	"""

	def __init__(	self, chromosomeShape,
					errorFunction,
					elitism = 1,
					populationSize = 25, 
					mutationProbability  = .1, 
					mutationScale = .5,
					numIterations = 10000, 
					errorTreshold = 1e-6
					): 

		self.populationSize = populationSize # size of the population of units
		self.p = mutationProbability # probability of mutation
		self.numIter = numIterations # maximum number of iterations
		self.e = errorTreshold # threshold of error while iterating
		self.f = errorFunction # the error function (reversely proportionl to fitness)
		self.keep = elitism  # number of units to keep for elitism
		self.k = mutationScale # scale of the gaussian noise

		self.i = 0 # iteration counter

		# initialize the population randomly from a gaussian distribution
		# with noise 0.1 and then sort the values and store them internally

		self.population = []
		for _ in range(populationSize):
			chromosome = np.random.randn(chromosomeShape) * 0.1

			fitness = self.calculateFitness(chromosome)
			self.population.append((chromosome, fitness))

		# sort descending according to fitness (larger is better)
		self.population = sorted(self.population, key=lambda t: -t[1])

		self.annealedK = self.k
		self.annealing = pow(0.01 / self.k, 1.0 / (self.numIter - 1))
		self.thresholdReached = False
	
	def step(self):	
		"""
			Run one iteration of the genetic algorithm. In a single iteration,
			you should create a whole new population by first keeping the best
			units as defined by elitism, then iteratively select parents from
			the current population, apply crossover and then mutation.

			The step function should return, as a tuple: 
				
			* boolean value indicating should the iteration stop (True if 
				the learning process is finished, False othwerise)
			* an integer representing the current iteration of the 
				algorithm
			* the weights of the best unit in the current iteration

		"""
		
		self.i += 1
		nextPopulation = []

		if self.keep == 1:
			nextPopulation.append(self.best())
		elif self.keep:
			nextPopulation.extend(self.bestN(self.keep))

		while len(nextPopulation) < self.populationSize:
			first, second = self.selectParents()
			#while first is second:
			#	first, second = self.selectParents()
			chromosome = self.crossover(first[0], second[0])
			self.mutate(chromosome)
			fitness = self.calculateFitness(chromosome)
			nextPopulation.append((chromosome, fitness))

		nextPopulation = sorted(nextPopulation, key=lambda t: -t[1])
		self.population = nextPopulation
		self.annealedK *= self.annealing
		return (self.thresholdReached or self.i == self.numIter, self.i, self.population[0][0])


	def calculateFitness(self, chromosome):
		"""
			Implement a fitness metric as a function of the error of
			a unit. Remember - fitness is larger as the unit is better!
		"""
		chromosomeError = self.f(chromosome)
		if chromosomeError < self.e:
			self.thresholdReached = True
		return 1.0 / chromosomeError

	def bestN(self, n):		
		"""
			Return the best n units from the population
		"""
		best = []
		for i in range(n):
			best.append(self.population[0])
		return best

	def best(self):
		"""
			Return the best unit from the population
		"""
		return self.population[0]

	def selectParents(self):
		"""
			Select two parents from the population with probability of 
			selection proportional to the fitness of the units in the
			population		
		"""
		fitnessSum = 0.0
		for solution in self.population:
			fitnessSum += solution[1]

		first = None
		value = np.random.uniform() * fitnessSum
		for solution in self.population:
			value -= solution[1]
			if value < 0.0:
				first = solution
				break
		if not first:
			first = solution

		second = None
		value = np.random.uniform() * fitnessSum
		for solution in self.population:
			value -= solution[1]
			if value < 0.0:
				second = solution
				break
		if not second:
			second = solution

		return first, second

	def crossover(self, p1, p2): 
		"""
			Given two parent units p1 and p2, do a simple crossover by 
			averaging their values in order to create a new child unit
		"""
		N = len(p1)
		child = np.zeros(N)
		for i in range(N):
			child[i] = (p1[i] + p2[i]) / 2.0
		return child

	def mutate(self, chromosome):
		"""
			Given a unit, mutate its values by applying gaussian noise
			according to the parameter k
		"""
		count = int(self.p * len(chromosome))
		for i in range(count):
			chromosome[i] += np.random.normal(0.0, self.annealedK)
		return chromosome


