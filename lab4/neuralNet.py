from networkLayers import *
from transferFunctions import * 

class NeuralNetwork(object):
	"""
		A class which streamlines the output through layers and error calculation.
	"""

	def __init__(self):
		"""
			Initialize the layers of the neural network to an empty array.
		"""
		self.layers = []

	def addLayer(self, layer):
		"""
			Add a layer to the neural network in sequential fashion.
		"""
		self.layers.append(layer)

	def output(self, x): 
		"""
			Calculate the output for a single input instance x (one row from 
			the training or test set)
		"""
		current = x
		for layer in self.layers:
			current = layer.output(current)
		return current

	def outputs(self, X):
		"""
			For a given vector of input instances X (the training or test set), 
			return the vector of outputs for all the input instances.
		"""
		N = len(X)
		outputs = np.zeros(N)
		for i in range(N):
			outputs[i] = (self.output(X[i]))
		return outputs

	def error(self, prediction, y):
		"""
			Calculates the error for a single example in the train/test set.
			The default error is MSE (mean square error).
		"""
		return pow(prediction - y, 2)


	def total_error(self, predictions, Y): 
		"""
			Calculates the total error for ALL the examples in the train/test set.
		"""
		sum = 0.0
		N = len(predictions)
		for i in range(N):
			sum += self.error(predictions[i], Y[i])
		return sum / N

	def forwardStep(self, X, Y):
		"""
			Run the inputs X (train/test set) through the network, and calculate
			the error on the given true target function values Y
		"""
		return self.total_error(self.outputs(X), Y)

	def size(self):
		"""
			Return the total number of weights in the network
		"""
		totalSize = 0 
		for layer in self.layers: 
			totalSize += layer.size()
		return totalSize

	def getWeightsFlat(self):
		"""
			Return a 1-d representation of all the weights in the network
		"""
		flatWeights = np.array([])
		for layer in self.layers: 
			flatWeights = np.append(flatWeights, layer.getWeightsFlat())
		return flatWeights

	def setWeights(self, flat_vector):
		"""
			Set the weights for all layers in the network
		"""
		# first layers come first in the flat vector
		for layer in self.layers:
			layer_weights = flat_vector[:layer.size()]
			layer.setWeights(layer_weights)
			flat_vector = flat_vector[layer.size():]
