
"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
from logic import *

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
        state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
        actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def miniWumpusSearch(problem): 
    """
    A sample pass through the miniWumpus layout. Your solution will not contain 
    just three steps! Optimality is not the concern here.
    """
    from game import Directions
    e = Directions.EAST 
    n = Directions.NORTH
    return  [e, n, n]

def logicBasedSearch(problem):
    """

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    (1, 1)
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    False
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    ((1, 2), 'North', 1)
    ((2, 1), 'East', 1)

    print "Does the Wumpus's stench reach my spot?",
               \ problem.isWumpusClose(problem.getStartState())
    False

    print "Can I sense the chemicals from the pills?", 
               \ problem.isPoisonCapsuleClose(problem.getStartState())
    False

    print "Can I see the glow from the teleporter?", 
               \ problem.isTeleporterClose(problem.getStartState())
    False
    
    (the slash '\\' is used to combine commands spanning through multiple lines - 
    you should remove it if you convert the commands to a single line)
    
    Feel free to create and use as many helper functions as you want.

    A couple of hints: 
        * Use the getSuccessors method, not only when you are looking for states 
        you can transition into. In case you want to resolve if a poisoned pill is 
        at a certain state, it might be easy to check if you can sense the chemicals 
        on all cells surrounding the state. 
        * Memorize information, often and thoroughly. Dictionaries are your friends and 
        states (tuples) can be used as keys.
        * Keep track of the states you visit in order. You do NOT need to remember the
        transitions - simply pass the visited states to the 'reconstructPath' method
        in the search problem. Check logicAgents.py and search.py for implementation.
    """
    return logicBasedAlgorithm(problem).run()

class logicBasedAlgorithm():

    def __init__(self, problem):
        self.problem = problem
        self.closedSet = set()
        self.knowledge = set()
        self.cache = {}

    def run(self):
        problem = self.problem
        knowledge = self.knowledge
        # array in order to keep the ordering
        visitedStates = []
        startState = problem.getStartState()

        stateQueue = MultiQueue()
        stateQueue.pushUnknownState(startState)
        while True:
            current = stateQueue.pop()
            if not current:
                print "I am surrounded by death - waiting for help from Pacearth"
                break
            if current in self.closedSet:
                continue
            visitedStates.append(current)
            if problem.isGoalState(current):
                return problem.reconstructPath(visitedStates)
            self.closedSet.add(current)

            successors = problem.getSuccessors(current)
            self.removeClosed(successors)
            self.expandKnowledge(current, successors)

            for successor in successors:
                if self.hasBeenDeduced(Labels.WUMPUS, successor):
                    continue
                if self.hasBeenDeduced(Labels.POISON, successor):
                    continue
                if self.hasBeenDeduced(Labels.TELEPORTER, successor):
                    stateQueue.pushTeleporter(successor[0])
                elif self.hasBeenDeduced(Labels.SAFE, successor):
                    stateQueue.pushSafeState(successor[0])
                else:
                    knowledge.add(Clause({
                        Literal(Labels.SAFE, successor[0]),
                        Literal(Labels.WUMPUS, successor[0]),
                        Literal(Labels.POISON, successor[0])
                    }))
                    stateQueue.pushUnknownState(successor[0])
        return problem.reconstructPath(visitedStates)

    def removeClosed(self, successors):
        toRemove = set()
        for successor in successors:
            if successor[0] in self.closedSet:
                toRemove.add(successor)
        for successor in toRemove:
            successors.remove(successor)

    def expandKnowledge(self, current, successors):
        problem = self.problem
        self.addToKnowledge(problem.isWumpusClose(current), successors, Labels.WUMPUS)
        self.addToKnowledge(problem.isPoisonCapsuleClose(current), successors, Labels.POISON)
        self.addToKnowledge(problem.isTeleporterClose(current), successors, Labels.TELEPORTER)
        if not problem.isWumpusClose(current) and not problem.isPoisonCapsuleClose(current):
            for successor in successors:
                self.knowledge.add(Clause(Literal(Labels.SAFE, successor[0])))
        self.knowledge.add(Clause(Literal(Labels.SAFE, current)))

    def addToKnowledge(self, condition, successors, label):
        if condition:
            literals = set()
            for successor in successors:
                literals.add(Literal(label, successor[0]))
            self.knowledge.add(Clause(literals))
        else:
            for successor in successors:
                self.knowledge.add(Clause(Literal(label, successor[0], True)))

    def hasBeenDeduced(self, label, successor):
        cacheLabel = self.cache.get(successor[0])
        if cacheLabel == label:
            return True
        resolved = resolution(self.knowledge, Clause(Literal(label, successor[0])))
        if resolved:
            self.cache[successor[0]] = label
            return True
        return False

class MultiQueue:

    def __init__(self):
        self.teleporter = None
        self.safeStates = util.PriorityQueueWithFunction(stateWeight)
        self.safeStateSet = set()
        self.unknownStates = util.PriorityQueueWithFunction(stateWeight)
        self.unknownStateSet = set()

    def pop(self):
        if self.teleporter:
            return self.teleporter
        if not self.safeStates.isEmpty():
            return self.safeStates.pop()
        if not self.unknownStates.isEmpty():
            return self.unknownStates.pop()
        return None

    def pushTeleporter(self, state):
        self.teleporter = state

    def pushSafeState(self, state):
        if state not in self.safeStateSet:
            self.safeStateSet.add(state)
            self.safeStates.push(state)

    def pushUnknownState(self, state):
        if state not in self.unknownStateSet:
            self.unknownStateSet.add(state)
            self.unknownStates.push(state)

# Abbreviations
lbs = logicBasedSearch
